const express = require("express");

const app = express();

app.get("/", (req, res) => {
  return res.json({
    message: "boo 👻",
  });
});

const listener = app.listen(4200, () => {
  const { port } = listener.address();
  console.log(`Listening on port ${port}`);
});
